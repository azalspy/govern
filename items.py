from azalspy.health.helpers import *

###################################################################################################

class SggItem(BaseItem):
    title = scrapy.Field()
    data = scrapy.Field()

class SgggovmaLaw(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

###################################################################################################

class Association(BaseItem):
    alias = scrapy.Field()
    title = scrapy.Field()

    phone = scrapy.Field(default=[])
    faxes = scrapy.Field(default=[])
    email = scrapy.Field()

    sites = scrapy.Field(default=[])
    pages = scrapy.Field(default=[])

    location = scrapy.Field()
    creation = scrapy.Field()

    objects = scrapy.Field(default=[])
    mission = scrapy.Field()
    populat = scrapy.Field()
    activ_p = scrapy.Field(default=[])
    activ_s = scrapy.Field(default=[])

###################################################################################################


